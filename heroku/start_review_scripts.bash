#!/bin/bash -ex

PROJECT_NAME='registrar'

curl -X POST https://api.heroku.com/apps -H "Accept: application/vnd.heroku+json; version=3" -H "Authorization: Bearer $HEROKU_API_KEY" -H "Content-Type: application/json" -d "{\"name\":\"$PROJECT_NAME-$CI_MERGE_REQUEST_IID\",\"region\":\"eu\"}"

git checkout -B $PROJECT_NAME-$CI_MERGE_REQUEST_IID
git status
git config user.email 'no-reply@universis.io'
git config user.name 'UniverSIS Project'

sed -i -e "s/\"callbackURL.*,/\"callbackURL\": \"https:\/\/$PROJECT_NAME-$CI_MERGE_REQUEST_IID.herokuapp.com\/auth\/callback\/index.html\",/" packages/registrar/src/assets/config/app.json

sed -i -e "s/\"logoutURL.*,/\"logoutURL\":\"https:\/\/users.universis.io\/logout?continue=https:\/\/$PROJECT_NAME-$CI_MERGE_REQUEST_IID.herokuapp.com\/#\/auth\/login\",/" packages/registrar/src/assets/config/app.json

sed -i -e "s/\"clientID.*,/\"clientID\": \"$CLIENT_ID\",/"  packages/registrar/src/assets/config/app.json

cp packages/registrar/src/assets/config/app.json packages/registrar/src/assets/config/app.production.json

git add -f packages/registrar/src/assets/config/app.production.json
git add .
git commit -m "Ready to push latest changes to new heroku review app"
git push -f https://heroku:$HEROKU_API_KEY@git.heroku.com/$PROJECT_NAME-$CI_MERGE_REQUEST_IID.git $PROJECT_NAME-$CI_MERGE_REQUEST_IID:master
