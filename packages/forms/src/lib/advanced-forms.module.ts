import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {MostModule} from '@themost/angular';
import {AdvancedFormsService} from './advanced-forms.service';
// import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import {FORMS_LOCALES} from './i18n';
import { FormioModule } from 'angular-formio';
import { AdvancedFormComponent } from './advanced-form.component';

@NgModule({
  imports: [
    
    HttpClientModule,
    MostModule,
    TranslateModule,
    FormioModule
  ],
  declarations: [AdvancedFormComponent],
  exports: [AdvancedFormComponent]
})
export class AdvancedFormsModule {

  constructor( @Optional() @SkipSelf() parentModule: AdvancedFormsModule, private _translateService: TranslateService) {
    environment.languages.forEach( language => {
      if (FORMS_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, FORMS_LOCALES[language], true);
      }
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AdvancedFormsModule,
      providers: [
        AdvancedFormsService
      ]
    };
  }
}
