/* tslint:disable quotemark */
/* tslint:disable max-line-length */
export const el = {
    "Forms": {
        required: '{{field}} είναι υποχρεωτικό',
        pattern: '{{field}} δεν ταιριάζει με το πρότυπο {{pattern}}',
        minLength: '{{field}} πρέπει να είναι μεγαλύτερο από {{length}} χαρακτήρες.',
        maxLength: '{{field}} πρέπει να είναι μικρότερο από {{length}} χαρακτήρες.',
        min: '{{field}} δεν μπορεί να να είναι μικρότερο από {{min}}.',
        max: '{{field}} δεν μπορεί να να είναι μεγαλύτερο από {{max}}.',
        maxDate: '{{field}} δεν μπορεί να είναι μεταγενέστερη της  {{- maxDate}}',
        minDate: '{{field}} δεν μπορεί να είναι προγενέστερη της {{- minDate}}',
        invalid_email: '{{field}} πρέπει να είναι έγκυρο email.',
        invalid_url: '{{field}} πρέπει να είναι έγκυρο url.',
        invalid_regex: '{{field}} δεν ταιριάζει με το πρότυπο {{regex}}.',
        invalid_date: '{{field}} δεν είναι έγκυρη ημερομηνία.',
        invalid_day: '{{field}} δεν είναι έγκυρη ημέρα.',
        mask: '{{field}} δεν ταιριάζει με το πρότυπο.',
        'Please enter a 10 digit phone number': 'Παρακαλώ εισάγεται ένα δεκαψήφιο αριθμό τηλεφώνου',
        complete: 'Υποβολή Ολοκληρώθηκε',
        error: 'Παρακαλώ διορθώστε τα παρακάτω λάθη πριν την υποβολή.',
        "Submit": "Υποβολή",
        "Contact Person": "Επαφή",
        "Department Name": "Ονομα Τμήματος",
        "Faculty Name": "Ονομα Σχολής",
        "Department ID": "Κωδικός Τμήματος",
        "Semesters": "Εξάμηνα",
        "Study Level": "Επίπεδο Σπουδών",
        "Alternative Contact Person": "Εναλλάκτική Επαφή",
        "Phone Number": "Τηλέφωνο",
        "Alternative Phone Number": "Εναλλακτικό τηλέφωνο",
        "Contact Details": "Στοιχεία Επικοινωνίας",
        "General Information": "Γενικές Πληροφορίες"
    }
};
/* tslint:enable quotemark */
/* tslint:enable max-line-length */
