import { TestBed } from '@angular/core/testing';

import { AdvancedFormsService, TranslateServicePreProcessor } from './advanced-forms.service';
import {AdvancedFormsModule} from './advanced-forms.module';
import {MostModule} from '@themost/angular';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

describe('FormsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
        HttpClientModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/api/',
          options: {
            useResponseConversion: true,
            useMediaTypeExtensions: false
          }
        }),
        AdvancedFormsModule.forRoot()
    ]
  }));

  it('should be created', () => {
    const service: AdvancedFormsService = TestBed.get(AdvancedFormsService);
    expect(service).toBeTruthy();
  });
    it('should use FormsService.loadForm()', async () => {
        const service: AdvancedFormsService = TestBed.get(AdvancedFormsService);
        const form = await service.loadForm('local-departments/edit');
        expect(form).toBeTruthy();

        const testComponents = (components) => {
          components.forEach( component => {
            if (component.data && component.data.url) {
              expect(component.data.url).toMatch(/^\/api\//);
            }
            if (component.components) {
              testComponents(component.components);
            }
            if (component.columns) {
              component.columns.forEach( column => {
                if (column.components) {
                  testComponents(column.components);
                }
              });
            }
          });
        };

        testComponents(form.components);
    });

    it('should use TranslateServicePreProcessor.parse()', async () => {
      const service: AdvancedFormsService = TestBed.get(AdvancedFormsService);
      const translateService: TranslateService = TestBed.get(TranslateService);
      translateService.use('en');
      const form = {
        /* tslint:disable quotemark */
        /* tslint:disable max-line-length */
        components: [
          {
            "type": "button",
            "label": "Forms.Submit",
            "key": "submit",
            "disableOnInvalid": true,
            "input": true,
            "action": "url",
            "url": "LocalDepartments"
        }
        ]
      };
      /* tslint:enable quotemark */
/* tslint:enable max-line-length */
      expect(form).toBeTruthy();

      new TranslateServicePreProcessor(translateService).parse(form);

      const testComponents = (components) => {
        components.forEach( component => {
          if (component.label) {
            expect(component.label).toBe('Submit');
          }
          if (component.components) {
            testComponents(component.components);
          }
          if (component.columns) {
            component.columns.forEach( column => {
              if (column.components) {
                testComponents(column.components);
              }
            });
          }
        });
      };

      testComponents(form.components);
  });

});
