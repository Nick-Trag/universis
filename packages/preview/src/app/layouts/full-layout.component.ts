import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styles: [`
    button.dropdown-item {
      cursor: pointer;
    }
    .sidebar .nav-dropdown-items {
      padding-left: 1rem;
    }
  `],
  encapsulation: ViewEncapsulation.None
})
export class FullLayoutComponent implements OnInit {

  public navItems = [];

  constructor(private _router: Router) {
    //
  }

  ngOnInit(): void {
    // get sidebar navigation items
    const navigationItems = [];
    this._router.config.filter( route => {
      return route.data && route.data.show;
    }).forEach( route => {
      const navigationItem = {
        name: route.data.title,
        url: '/' + route.path,
        children: []
      };
      route.children.filter( child => {
        return child.data && child.data.show;
      }).forEach( child => {
        navigationItem.children.push({
          name: child.data.title,
          url: '/' + route.path + '/' + child.path
        });
      });
      navigationItems.push(navigationItem);
    });
    this.navItems = navigationItems;
  }

}
