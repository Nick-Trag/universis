import {Component, Input, OnInit, Output, ViewChild, ViewEncapsulation, EventEmitter, AfterViewInit, TemplateRef} from '@angular/core';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component';
import { ActivatedRoute } from '@angular/router';
import { template, at } from 'lodash';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { UserStorageService } from 'packages/common/src/shared/services/user-storage';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from './advanced-filter-value-provider.service';
import { AdvancedTableSearchBaseComponent } from './advanced-table-search-base';

@Component({
    selector: 'app-advanced-table-search',
    templateUrl: './advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class AdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent implements OnInit, AfterViewInit  {

    @Input() table: AdvancedTableComponent;
    @Input() collapsed = true;
    @Input() allowSave = true;
    // @Input() filter: any = {};

    // @Output() filterToAplly = new EventEmitter<any>();

    modalRef: BsModalRef;
    storageKey: any;
    storageValue: any;
    savedSearchLists: any = [];
    selectedList: any = null;

    constructor( protected _context: AngularDataContext,
                 protected _activatedRoute: ActivatedRoute,
                 protected datePipe: DatePipe,
                 private _modalService: BsModalService,
                 private _userStorage: UserStorageService,
                 private _translate: TranslateService,
                 private _advancedSearchValueProvider: AdvancedFilterValueProvider) { super();}

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
      this.loadSavedSearchList();
    }

    onSearchKeyDown(event: any) {
        if (this.table && event.keyCode === 13) {
            this.table.search((<HTMLInputElement>event.target).value);
            return false;
        }
    }

    transformDate(date) {
        date = this.datePipe.transform(date, 'MM/dd/yyyy'); // whatever format you need.
        return date;
    }

    convertToString(nameFilter) {
        let name = '';
        if (nameFilter != null) {
            name = nameFilter.toString();
        }
        return name;
    }

//  Load saved criteria searches user has stored before
    loadSavedSearchList(): any {
      this.storageKey = 'registrar/tables/' + this.table.config.model + '/searches';
      this._userStorage.getItem(this.storageKey).then((data) => {
        this.savedSearchLists = data.value;
        if ( Array.isArray(this.savedSearchLists) && this.savedSearchLists.length !== 0 )  {
          this.collapsed = false;
        }
        if (this.table && this.table.config.searches && this.table.config.searches.length) {
          this.savedSearchLists.push.apply(this.savedSearchLists, this.table.config.searches.map ( x => {
          x.readOnly = true;
          x.name = this._translate.instant(x.name);
          return x;
          }));
          }
      });
    }

    onSelectLoadSearch(event: any): boolean {
      if (this.selectedList) {
        const listFound = this.searchInListByName(this.selectedList.name);
        if (listFound) {
          this.selectedList = listFound;
          this.filter = this.selectedList.filter;
          // this.filterToAplly.emit(this.selectedList.filter);
          this.collapsed = false;
          return true;
        }
      }
//    In case of error
      this.filter = {};
      // this.filterToAplly.emit({});
      return false;
    }

    searchInListByName (name: String): any {
      if ( Array.isArray(this.savedSearchLists) ) {
        const listFound = this.savedSearchLists.find( x => x.name === name);
        if ( listFound ) {
          return listFound;
        } else {
          return false;
        }
      }
    }

//  Update list item contents with given content (Search by object name property)
    updateSavedSearchListItemByName ( name: String, newItem: any ) {
      const itemInList = this.savedSearchLists.find( x => x.name === name );
      const index = this.savedSearchLists.indexOf(itemInList);
      this.savedSearchLists[index] = newItem;
      this.selectedList = this.savedSearchLists[index];
    }

//  Update list item contents with given content (Search by object)
    updateSavedSearchListItemByObject ( itemInList: any, newItem: any ) {
      const index = this.savedSearchLists.indexOf(itemInList);
      this.savedSearchLists[index] = newItem;
      this.selectedList = this.savedSearchLists[index];
    }

//  Delete list item from list
    deleteItemFromSavedSearchListByObject ( itemInList: any) {
      this.savedSearchLists = this.savedSearchLists.filter(item => item !== itemInList);
      this.selectedList = null;
      this.filter = {};
      // this.filterToAplly.emit({});
      this.apply();
      this.uploadListOnUserStorage();
    }

//  User chose to save its advanced form search criteria
    saveSearch( templateToLoad: TemplateRef<any>, newList: boolean = true) {
      if ( this.table.config.model ) {
//      Set the corresponding key to its list and show a confirmation modal window
        this.storageKey = 'registrar/tables/' + this.table.config.model + '/searches';

        if (this.filter && Object.keys(this.filter).length !== 0) {
//      User wants to save an existing list from the dropdown
          if ( !newList && this.selectedList ) {
            const newItem = { 'name' :  this.selectedList.name, 'filter' : { ... this.filter } };
            this.updateSavedSearchListItemByObject ( this.selectedList, newItem );
            this.uploadListOnUserStorage();
//      User wants to save a new list
          } else {
            this.modalRef = this._modalService.show(templateToLoad);
          }
        } else {
//        User did not define any criteria
          alert(this._translate.instant('AdvancedSearch.Modal.EmptyCriteriaList'));
        }
      } else {
//      Dev: Should define list name list's table component
        alert(this._translate.instant('AdvancedSearch.Modal.NoListDefined'));
      }
    }

//  User Accepted => Save list and Close modal
    confirmModal( saveFormName ): void {
//    name given and list name is unique
      if ( saveFormName && !(this.searchInListByName(saveFormName)) ) {
        const newInsertedValue = { 'name' :  saveFormName, 'filter' : { ... this.filter } };
        if ( this.savedSearchLists && this.savedSearchLists.length !== 0 ) {
          this.savedSearchLists.push(newInsertedValue);
          this.storageValue = this.savedSearchLists;
          this.selectedList = newInsertedValue;
        } else {
          this.storageValue = [newInsertedValue];
          this.savedSearchLists = this.storageValue;
          this.selectedList = newInsertedValue;
        }
        this.uploadListOnUserStorage();
        this.modalRef.hide();
      } else if ( saveFormName && (this.searchInListByName(saveFormName)) ) {
//      User should insert a different name cause this one is already in use
        alert(this._translate.instant('AdvancedSearch.Modal.TitleOnListAlreadyCreated'));
      } else {
//      User did not insert any text
        alert(this._translate.instant('AdvancedSearch.Modal.TitleOnUserError'));
      }
    }

//  User declined => Close modal
    declinecModal(): void {
      this.modalRef.hide();
    }

//  Upload user's criteria on User Storage
    uploadListOnUserStorage() {
      this._userStorage.setItem(this.storageKey, this.savedSearchLists.filter(x => {
        return !x.readOnly;
      }));
    }

    apply() {
        if (Array.isArray(this.table.config.criteria)) {
            const expressions = [];
            this.table.config.criteria.forEach(x => {
                if (this.filter[x.name]) {
                    const nameFilter = this.convertToString(this.filter[x.name]);
                    if (nameFilter.includes('GMT')) {
                        const newDate = this.transformDate(this.filter[x.name]);
                        expressions.push(template(x.filter)({
                            value: newDate
                        }));
                    } else if (this.filter[x.name] !== 'undefined') {
                      expressions.push(template(x.filter)(Object.assign({
                          value: this.filter[x.name]
                      }, this._advancedSearchValueProvider.values)));
                    }
                }
            });
            if (expressions || expressions.length) {
                // create client query
                const query = this._context.model(this.table.config.model).asQueryable();
                query.setParam('filter', expressions.join(' and ')).prepare();
                this.table.query = query;
                this.table.fetch();
            }
        }
        return false;
    }

    async asyncOnChange(event: any) {
        if (Array.isArray(this.table.config.criteria)) {
            const expressions = [];
            const values = await this._advancedSearchValueProvider.getValues();
            this.table.config.criteria.forEach((x) => {
                if (this.filter[x.name]) {
                    const nameFilter = this.convertToString(this.filter[x.name]);
                    if (nameFilter.includes('GMT')) {
                        const newDate = this.transformDate(this.filter[x.name]);
                        expressions.push(template(x.filter)({
                            value: newDate
                        }));
                    } else if (this.filter[x.name] !== 'undefined') {
                        expressions.push(template(x.filter)(Object.assign({
                            value: this.filter[x.name]
                        }, values)));
                    }
                }
            });
            if (expressions || expressions.length) {
                // create client query
                const query = this._context.model(this.table.config.model).asQueryable();
                query.setParam('filter', expressions.join(' and ')).prepare();
                this.table.query = query;
                this.table.fetch();
            }
        }
    }

    onChange(event: any) {
        return this.asyncOnChange(event);
    }
}
