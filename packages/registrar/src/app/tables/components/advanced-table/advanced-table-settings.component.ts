import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {ActivatedTableService} from '../../tables.activated-table.service';
import {UserStorageService} from '@universis/common/shared/services/user-storage';
import {forEach} from '@angular/router/src/utils/collection';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn} from '@angular/forms';

@Component({
  selector: 'app-advanced-table-settings',
  templateUrl: './advanced-table-settings.component.html',
  encapsulation: ViewEncapsulation.None
})

export class AdvancedTableSettingsComponent implements OnInit, AfterViewInit {

  @Input() table: AdvancedTableComponent;
  @ViewChild('modal_settings_template') modal_settings_template: ElementRef;
  modalRef: BsModalRef;
  storageKey: any;
  params: any = this._activatedRoute.params;
  private savedTableSettings: any;
  form: FormGroup;
  columns = [];
  tableColumns: any;
  activeTableConfig: any;

  constructor(protected _context: AngularDataContext,
              protected _activatedRoute: ActivatedRoute,
              private elRef: ElementRef,
              private modalService: BsModalService,
              private userStorage: UserStorageService,
              private formBuilder: FormBuilder,
              private _activatedTable: ActivatedTableService) {
  }
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.activeTableConfig = this._activatedTable.activeTable.getConfig();
    this.storageKey = 'registrar/settings/' + this.activeTableConfig.title + '/table/columns';
    this.form = this.formBuilder.group({columns: new FormArray([], this.minSelectedColumns(1))});
    this.userStorage.getItem(this.storageKey).then((data) => {
      if (data) {
        this.savedTableSettings = data.value;
        if (this.savedTableSettings) {
          if (this._activatedTable.activeTable) {
            this._activatedTable.activeTable.handleColumns(this.savedTableSettings);
          }
        }
        this.addCheckboxes();
      }
    });
  }
  private addCheckboxes() {
    this.tableColumns = this.activeTableConfig.columns.slice(1);
    this.tableColumns.forEach ((column, i) => {
      if (!column.hidden) {
        const control = {
          id: column.name,
          name: column.title
        };
        this.columns.push(control);
      }
    });
    if (this.savedTableSettings) {
      this.columns.forEach ((column, i) => {
        column.check = false;
        column.check = this.savedTableSettings.includes(column.id);
      });
      this.columns.map((column, i) => {
        const control = new FormControl(column.check);
        (this.form.controls.columns as FormArray).push(control);
      });
    }
  }

  openTableSettings(table_settings_modal: TemplateRef<any>) {
    this.modalRef = this.modalService.show(table_settings_modal);
  }

  saveTableSettings() {
    const selectedColumnsIds = this.form.value.columns
      .map((v, i) => v ? this.columns[i].id : null)
      .filter(v => v !== null);
    this.userStorage.setItem(this.storageKey, selectedColumnsIds);
    if (this._activatedTable.activeTable) {
      this._activatedTable.activeTable.handleColumns(selectedColumnsIds);
    }
    this.modalRef.hide();
  }

  dismiss() {
    this.modalRef.hide();
  }

  minSelectedColumns(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);

      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }
}
