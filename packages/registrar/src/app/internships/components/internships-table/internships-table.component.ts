import { Component, OnInit, ViewChild } from '@angular/core';
import * as INTERNSHIPS_LIST_CONFIG from './internships-table.config.json';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-internships-table',
  templateUrl: './internships-table.component.html',
  styleUrls: ['./internships-table.component.scss']
})
export class InternshipsTableComponent implements OnInit {

  public readonly config = INTERNSHIPS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
