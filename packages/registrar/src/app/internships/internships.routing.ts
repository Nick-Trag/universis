import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InternshipsHomeComponent } from './components/internships-home/internships-home.component';
import { InternshipsTableComponent } from './components/internships-table/internships-table.component';
import { InternshipsPreviewComponent } from './components/internships-preview/internships-preview.component';
import { InternshipsRootComponent } from './components/internships-root/internships-root.component';
import { InternshipsPreviewGeneralComponent } from './components/internships-preview-general/internships-preview-general.component';

const routes: Routes = [
    {
        path: '',
        component: InternshipsHomeComponent,
        data: {
            title: 'Internships'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: InternshipsTableComponent,
                data: {
                    title: 'Internships List'
                }
            },
            {
                path: 'active',
                component: InternshipsTableComponent,
                data: {
                    title: 'Active Internships'
                }
            }
        ]
    },
    {
        path: ':id',
        component: InternshipsRootComponent,
        data: {
            title: 'Internship Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: InternshipsPreviewComponent,
                data: {
                    title: 'Internship Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: InternshipsPreviewGeneralComponent,
                        data: {
                            title: 'Exams Preview General'
                        }
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class InternshipsRoutingModule {
}
