import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {ElementsModule} from '../elements/elements.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '../tables/tables.module';
import { SharedModule } from '@universis/common';
import { DepartmentsSharedModule } from './departments.shared';
import { DepartmentsRoutingModule } from './departments.routing';
import { DepartmentsTableComponent } from './components/departments-table/departments-table.component';
import { DepartmentsHomeComponent } from './components/departments-home/departments-home.component';
import { DepartmentsPreviewComponent } from './components/departments-preview/departments-preview.component';
import { DepartmentsPreviewGeneralComponent } from './components/departments-preview/departments-preview-general/departments-preview-general.component';
import { DepartmentsRootComponent } from './components/departments-root/departments-root.component';
import { MostModule } from '@themost/angular';
import { DepartmentsAdvancedTableSearchComponent } from './components/departments-table/departments-advanced-table-search.component';
import { AdvancedFormsModule } from '@universis/forms/lib/advanced-forms.module';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';

@NgModule({
    imports: [
        ElementsModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        TablesModule,
        SharedModule,
        DepartmentsSharedModule,
        DepartmentsRoutingModule,
        MostModule,
        AdvancedFormsModule,
        RegistrarSharedModule
    ],
  declarations: [DepartmentsTableComponent,
                DepartmentsHomeComponent,
                DepartmentsPreviewComponent,
                DepartmentsPreviewGeneralComponent,
                DepartmentsRootComponent,
                DepartmentsAdvancedTableSearchComponent
                ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DepartmentsModule { }
