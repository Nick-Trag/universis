import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component.js';
import * as REGISTRATIONS_LIST_CONFIG from './departments-table.config.json';
@Component({
  selector: 'app-departments-table',
  templateUrl: './departments-table.component.html'
})
export class DepartmentsTableComponent implements OnInit {

  public readonly config = REGISTRATIONS_LIST_CONFIG;

  @ViewChild('table') departments: AdvancedTableComponent;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.departments.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
