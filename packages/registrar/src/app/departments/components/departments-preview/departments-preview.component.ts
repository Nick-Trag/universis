import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-departments-preview',
  templateUrl: './departments-preview.component.html'
})
export class DepartmentsPreviewComponent implements OnInit {

  @Input() departments: any;

   constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private translate: TranslateService) {
  }

  async ngOnInit() {

    this.departments = await this._context.model('LocalDepartments')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .getItem();
  }

}
