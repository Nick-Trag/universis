import { Component, OnInit, ViewChild } from '@angular/core';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component';
import * as USERS_LIST_CONFIG from './users-table.config.json';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  public readonly config = USERS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
