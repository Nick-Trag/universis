import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as SCHOLARSHIPS_STUDENTS_LIST_CONFIG from './scholarships-preview-students.config.json';

@Component({
  selector: 'app-scholarships-preview-students',
  templateUrl: './scholarships-preview-students.component.html'
})
export class ScholarshipsPreviewStudentsComponent implements OnInit {

  public readonly config = SCHOLARSHIPS_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.students.query =  this._context.model('StudentScholarships')
      .where('scholarship').equal(this._activatedRoute.snapshot.params.id)
      .expand('Student')
      .prepare();
    this.students.config = AdvancedTableConfiguration.cast(SCHOLARSHIPS_STUDENTS_LIST_CONFIG);
  }

}
