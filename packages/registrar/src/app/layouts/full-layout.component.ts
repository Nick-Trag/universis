import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {AppSidebarService} from '../registrar-shared/services/app-sidebar.service';
import {ConfigurationService, UserService} from '@universis/common';
import {ActivatedRoute} from '@angular/router';
import {ActiveDepartmentService} from '../registrar-shared/services/activeDepartmentService.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styles: [
      `
          header.app-header.navbar {
              justify-content: unset;
          }

          button.dropdown-item {
              cursor: pointer;
          }

          input.form-search {
              outline: none;
          }

          .navbar-nav-content{
              width: 60vmin;
              overflow: hidden !important;
          }

          .navbar-nav-content > li {
              width: 28%;
          }

          .dropdown-menu .divider {
              width: 100%;
              margin: 9px 1px 5px;
              overflow: hidden;
              border-bottom: 1px solid #e8e1e1;
          }

          li.dropdown-submenu:hover > ul.dropdown-menu {
              display: block;
              overflow: visible !important ;
          }

          ul.navbar-nav-content > li.dropdown-submenu:hover{
              background-color: #f0f3f5;
          }

          .tab-content .tab-pane {
              display: none;
          }

          .tab-content .tab-pane.active {
              display: block;
          }

          .tab-content, .nav-tabs {
              border: 0;
          }

          .tab-pane a {
              cursor: pointer;
          }

          ul.nav.nav-tabs > li > a.active.show {
              font-weight: bold;
          }

          .border-right {
              border-right: 1px solid #f0f3f5 !important;
          }

          .close {
              opacity: 1;
              position: absolute;
              top: 10px;
              right: 10px;
              z-index: 9;
          }

          .departement-nav,
          .departement-nav > li > a {
              width: 420px;
          }
          
          @media screen and (max-width: 375px) {
              .departement-nav,
              .departement-nav > li > a{
                  width: 220px;
              }

              .departement-nav > li{
                  position: inherit;
              }
          }
    `
  ]
})
export class FullLayoutComponent implements OnInit, OnDestroy {

  constructor(private _context: AngularDataContext,
              private _configurationService: ConfigurationService,
              private _userService: UserService,
              private _activatedRoute: ActivatedRoute,
              private _appSidebarService: AppSidebarService,
              private _activeDepartmentService: ActiveDepartmentService) {

  }

  @ViewChild('appSidebarNav') appSidebarNav: any;
  public status = { isOpen: false };
  public user;
  public today = new Date();
  public currentLang;
  public languages;
  public model: any;
  public activeDepartment: any;
  public _activeDepartmentSource: Subscription;
  public search = [];
  public isCollapsed = true;
  public applicationImage;

  async ngOnInit() {
    // get sidebar navigation items
    this.appSidebarNav.navItems = this._appSidebarService.navigationItems;
    // get current user
    this._userService.getUser().then( user => {
      this.user = user;
    });
    // get current language
    this.currentLang = this._configurationService.currentLocale;
    // get languages
    this.languages = this._configurationService.settings.i18n.locales;

    this.model = await this._context.model('LocalDepartments')
      .asQueryable()
      .take(-1)
      .getItems();

    // Source object with binding for active department
    this._activeDepartmentSource = this._activeDepartmentService.departmentChange
      .subscribe(activeDepartment => this.activeDepartment = activeDepartment)

    // local object for active department
    this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    if (!this.activeDepartment) {
      if (this.model && this.model.length > 0) {
        this.activeDepartment = this.model[0];
      }
    }
    // get path of brand logo
    this.applicationImage = this._configurationService.settings.app && this._configurationService.settings.app.image;
  }

  ngOnDestroy() {

    // prevent memory leak when component is destroyed
    if (this._activeDepartmentSource) {
      this._activeDepartmentSource.unsubscribe();
    }
  }

  changeLanguage(lang) {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }

  changeDepartment(department) {
    this.activeDepartment = department;
     this._activeDepartmentService.setActiveDepartment(this.activeDepartment);
  }
}
