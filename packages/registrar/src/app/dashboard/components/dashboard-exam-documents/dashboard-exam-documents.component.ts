import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-dashboard-exam-documents',
  templateUrl: './dashboard-exam-documents.component.html',
  styleUrls: ['./dashboard-exam-documents.component.scss']
})
export class DashboardExamDocumentsComponent implements OnInit {

  public uploadGrades: any;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.uploadGrades = await this._context.model('ExamDocumentUploadActions')
      .where('actionStatus/alternateName ').equal('ActiveActionStatus')
      .and('additionalResult').notEqual(null)
      .expand('owner,object($expand=year,examPeriod,course)')
      // .expand('additionalResult,createdBy')
      .orderByDescending('dateModified')
      .take(3)
      .getList();
  }

}
