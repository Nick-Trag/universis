import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-dashboard-pending-student-requests',
  templateUrl: './dashboard-pending-student-requests.component.html',
  styleUrls: ['./dashboard-pending-student-requests.component.scss']
})
export class DashboardPendingStudentRequestsComponent implements OnInit {

  public lastRequests: any;
  
  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.lastRequests = await this._context.model('StudentRequestActions')
      .where('actionStatus/alternateName ').equal('ActiveActionStatus')
      .orderByDescending('dateModified')
      .take(3)
      .getList();
  }

}
