import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-currentgrades',
  templateUrl: './students-overview-currentgrades.component.html',
  styleUrls: ['./students-overview-currentgrades.component.scss']
})
export class StudentsOverviewCurrentgradesComponent implements OnInit {

  public recentGrades: any;
  public lastYearPeriod: any;
  @Input() studentId: number;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.lastYearPeriod = await this._context.model('students/' + this.studentId + '/courses')
      .select( 'gradePeriod', 'gradeYear')
      .where('gradeYear').notEqual(null)
      .and('gradePeriod').notEqual(null)
      .groupBy('gradePeriod', 'gradeYear')
      .orderByDescending('gradeYear')
      .thenByDescending('gradePeriod')
      .take(1)
      .getItem();

    if (this.lastYearPeriod) {
      this.recentGrades = await this._context.model('students/' + this.studentId + '/courses')
        .where('gradeYear/id').equal(this.lastYearPeriod.gradeYear.id)
        .and('gradePeriod/id').equal(this.lastYearPeriod.gradePeriod.id)
        .expand('course($expand=instructor)')
        .take(3)
        .getItems();
    }
  }

}
