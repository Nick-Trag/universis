import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-requests',
  templateUrl: './students-overview-requests.component.html',
  styleUrls: ['./students-overview-requests.component.scss']
})
export class StudentsOverviewRequestsComponent implements OnInit {

  public lastRequests: any;
  @Input() studentId: number;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.lastRequests = await this._context.model('StudentRequestActions')
      .where('student').equal(this.studentId)
      .orderByDescending('dateModified')
      .getItems();
  }

}
