import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-theses',
  templateUrl: './students-overview-theses.component.html',
  styleUrls: ['./students-overview-theses.component.scss']
})
export class StudentsOverviewThesesComponent implements OnInit {

  public theses: any;
  @Input() studentId: number;
  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.theses = await this._context.model('StudentTheses')
      .asQueryable()
      .where('student').equal(this.studentId)
      .getItems();
  }

}
