import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-profile',
  templateUrl: './students-overview-profile.component.html',
  styleUrls: ['./students-overview-profile.component.scss']
})
export class StudentsOverviewProfileComponent implements OnInit {

  public student;
  @Input() studentId: number;
//  Value to indicate whether the message send form should be visible or not (This is passed to child component)
  public showMessageForm = false;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.student = await this._context.model('Students')
      .where('id').equal(this.studentId)
      .expand('person($expand=gender), department, studyProgram')
      .getItem();
  }

/*
 *  This Functions toggles Message Send Form Visibility
 */
  enableMessages() {
    this.showMessageForm = !this.showMessageForm;
  }

/*
 *  This functions is used to receive message sent status from child component
 */
 onsuccesfulSend(succesful: boolean) {
//  Then toggles messge form visibility accordingly
    this.showMessageForm = !succesful;
  }

}
