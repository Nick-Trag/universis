import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-students-general',
  templateUrl: './students-general.component.html'
})
export class StudentsGeneralComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Students')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('person,studyProgram,user,department')
      .getItem();


  }

}
