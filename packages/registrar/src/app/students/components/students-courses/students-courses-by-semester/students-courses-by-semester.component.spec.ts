import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsCoursesBySemesterComponent } from './students-courses-by-semester.component';

describe('StudentsCoursesBySemesterComponent', () => {
  let component: StudentsCoursesBySemesterComponent;
  let fixture: ComponentFixture<StudentsCoursesBySemesterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsCoursesBySemesterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsCoursesBySemesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
