import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';


@Component({
  selector: 'app-students-scholarships',
  templateUrl: './students-scholarships.component.html',
  styleUrls: ['./students-scholarships.component.scss']
})
export class StudentsScholarshipsComponent implements OnInit {
  public model: any;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {  }


  async ngOnInit() {
    this.model = await this._context.model('StudentScholarships')
      .where('student').equal(this._activatedRoute.snapshot.params.id)
      .expand('scholarship($expand=scholarshipType,status,department,year)')
      .getItems();
  }

}
