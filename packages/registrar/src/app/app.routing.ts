import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {AuthModule, AuthGuard} from '@universis/common';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule',
        data: {
          model: 'Courses'
        }
      },
      {
        path: 'classes',
        loadChildren: './classes/classes.module#ClassesModule',
        data: {
          model: 'CourseClasses'
        }
      },
      {
        path: 'exams',
        loadChildren: './exams/exams.module#ExamsModule',
        data: {
          model: 'CourseExams'
        }
      },
      {
        path: 'instructors',
        loadChildren: './instructors/instructors.module#InstructorsModule',
        data: {
          model: 'Instructors',
          serviceParams: { '$expand': 'department($expand=organization),user' }
        }
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule',
        data: {
          model: 'Students'
        }
      },
      {
        path: 'study-programs',
        loadChildren: './study-programs/study-programs.module#StudyProgramsModule',
        data: {
          model: 'StudyPrograms'
        }
      },
      {
        path: 'requests',
        loadChildren: './requests/requests.module#RequestsModule',
        data: {
          model: 'StudentRequests'
        }
      },
      {
        path: 'theses',
        loadChildren: './theses/theses.module#ThesesModule',
        data: {
          model: 'Theses'
        }
      },
      {
        path: 'scholarships',
        loadChildren: './scholarships/scholarships.module#ScholarshipsModule',
        data: {
          model: 'Scholarships'
        }
      },
      {
        path: 'internships',
        loadChildren: './internships/internships.module#InternshipsModule',
        data: {
          model: 'Internships'
        }
      },
      {
        path: 'registrations',
        loadChildren: './registrations/registrations.module#RegistrationsModule',
        data: {
          model: 'StudentPeriodRegistrations'
        }
      },
      {
        path: 'departments',
        loadChildren: './departments/departments.module#DepartmentsModule',
        data: {
          model: 'LocalDepartments'
        }
      },
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
        paramsInheritanceStrategy: 'always'
      }), AuthModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
