import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule,TranslateService} from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {StudyProgramsPreviewFormComponent} from './components/study-programs-preview/study-programs-preview-general/study-programs-preview-form.component';
import { ProgramCoursePreviewFormComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-form.component';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent
  ],
  exports: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent
  ]
})
export class StudyProgramsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading study programs shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/study-programs.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
