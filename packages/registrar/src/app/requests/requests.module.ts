import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestsRoutingModule } from './requests.routing';
import { RequestsSharedModule } from './requests.shared';
import { RequestsTableComponent } from './components/requests-table/requests-table.component';
import { TablesModule } from '../tables/tables.module';
import { TranslateModule } from '@ngx-translate/core';
import { RequestsRootComponent } from './components/requests-root/requests-root.component';
import { RequestsHomeComponent } from './components/requests-home/requests-home.component';
import { RequestsEditComponent } from './components/requests-edit/requests-edit.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';
import { ModalModule } from 'ngx-bootstrap';
import { RequestsAdvancedTableSearchComponent } from './components/requests-table/requests-advanced-table-search.component';
import { MostModule } from '@themost/angular';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { RequestsDocumentsComponent } from './components/requests-documents/requests-documents.component';
import { RequestsDocumentsHomeComponent } from './components/requests-documents-home/requests-documents-home.component';

@NgModule({
  imports: [
    CommonModule,
    RequestsSharedModule,
    RequestsRoutingModule,
    TablesModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    ModalModule,
    MostModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [DatePipe],
  declarations: [
    RequestsHomeComponent,
    RequestsEditComponent,
    RequestsRootComponent,
    RequestsTableComponent,
    RequestsAdvancedTableSearchComponent,
    RequestsDocumentsComponent,
    RequestsDocumentsHomeComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RequestsModule { }
