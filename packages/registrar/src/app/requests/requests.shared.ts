import {NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {RequestsPreviewFormComponent} from './components/requests-preview-form/requests-preview-form.component';
import {StudentsSharedModule} from '../students/students.shared';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    StudentsSharedModule
  ],
  declarations: [
    RequestsPreviewFormComponent,
  ],
  exports: [
    RequestsPreviewFormComponent
  ]
})

export class RequestsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading requests shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/requests.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
