import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';
import {RequestsTableComponent} from './components/requests-table/requests-table.component';
import {RequestsRootComponent} from './components/requests-root/requests-root.component';
import {RequestsEditComponent} from './components/requests-edit/requests-edit.component';
import {RequestsDocumentsComponent} from './components/requests-documents/requests-documents.component';
import {RequestsDocumentsHomeComponent} from './components/requests-documents-home/requests-documents-home.component';


const routes: Routes = [
    {
    path: '',
    component: RequestsHomeComponent,
    data: {
      title: 'Requests'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: RequestsTableComponent,
        data: {
          title: 'Requests List'
        }
      },
      {
        path: 'active',
        component: RequestsTableComponent,
        data: {
          title: 'Active Requests'
        }
      }
    ]
  },
  {
    path: 'documents',
    component: RequestsDocumentsHomeComponent,
    data: {
      title: 'Requests Documents'
    },
    children: [
      {
        path: '',
        component: RequestsDocumentsComponent,
        data: {
          title: 'Requests Documents'
        }
      }
    ]
  },
  {
    path: ':id',
    component: RequestsRootComponent,
    data: {
      title: 'Request Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'edit'
      },
      {
        path: 'edit',
        component: RequestsEditComponent,
        data: {
          title: 'Request Edit'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class RequestsRoutingModule {
}
