import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {CoursesPreviewCourseInfoFormComponent} from './components/courses-preview-general/courses-preview-course-info-form.component';
import { CoursesOverviewClassesComponent } from './components/courses-overview/courses-overview-classes/courses-overview-classes.component';
import { CoursesOverviewStudyProgrammsComponent } from './components/courses-overview/courses-overview-study-programms/courses-overview-study-programms.component';
import { CoursesOverviewGeneralComponent } from './components/courses-overview/courses-overview-general/courses-overview-general.component';
import {CoursesOverviewExamsComponent} from './components/courses-overview/courses-overview-exams/courses-overview-exams.component';

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    CoursesPreviewCourseInfoFormComponent,
    CoursesOverviewClassesComponent,
    CoursesOverviewStudyProgrammsComponent,
    CoursesOverviewGeneralComponent,
    CoursesOverviewExamsComponent
  ],
  exports: [
    CoursesPreviewCourseInfoFormComponent,
    CoursesOverviewClassesComponent,
    CoursesOverviewStudyProgrammsComponent,
    CoursesOverviewGeneralComponent,
    CoursesOverviewExamsComponent
  ],
  providers: [
    CoursesOverviewClassesComponent,
    CoursesOverviewStudyProgrammsComponent,
    CoursesOverviewGeneralComponent,
    CoursesOverviewExamsComponent
  ]
})
export class CoursesSharedModule  implements OnInit {

  constructor(private _translateService: TranslateService) {
      this.ngOnInit().catch(err => {
          console.error('An error occurred while loading courses module');
          console.error(err);
      });
  }

  async ngOnInit() {
      // create promises chain
      const sources = environment.languages.map(async (language) => {
          const translations = await import(`./i18n/courses.${language}.json`);
          this._translateService.setTranslation(language, translations, true);
      });
      // execute chain
      await Promise.all(sources);
  }

}
