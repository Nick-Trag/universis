import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '../../../tables/components/advanced-table/advanced-table-search-base';

@Component({
    selector: 'app-courses-classes-advanced-table-search',
    templateUrl: './courses-classes-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class CoursesClassesAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    courseID: any = this._activatedRoute.snapshot.params.id;

    constructor(_context: AngularDataContext, private _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
