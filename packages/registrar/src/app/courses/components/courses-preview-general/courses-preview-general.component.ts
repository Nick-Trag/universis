import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-courses-preview-general',
  templateUrl: './courses-preview-general.component.html'
})
export class CoursesPreviewGeneralComponent implements OnInit {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('Courses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
      .getItem();
  }

}
