import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-courses-overview-classes',
  templateUrl: './courses-overview-classes.component.html',
  styleUrls: ['./courses-overview-classes.component.scss']
})
export class CoursesOverviewClassesComponent implements OnInit {
  @Input() currentYear: any;
  public courseId: any;
  public model: any;
  public currentCourseClasses: any;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
}


  async ngOnInit() {


    this.currentCourseClasses = await this._context.model('CourseClasses')
      .where('course').equal(this._activatedRoute.snapshot.params.id)
      .expand('period,status,instructors($expand=instructor),course($expand=department),students($select=courseClass,count(id) as total;$groupby=courseClass)')
      .and('year').equal({'$name': '$it/course/department/currentYear'})
      .getItems();

    this.courseId = this._activatedRoute.snapshot.params.id;
  }
}
