import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-courses-overview-study-programms',
  templateUrl: './courses-overview-study-programms.component.html',
  styleUrls: ['./courses-overview-study-programms.component.scss']
})
export class CoursesOverviewStudyProgrammsComponent implements OnInit {
  public courseId: any;
  public model: any;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('ProgramCourses')
      .where('course').equal(this._activatedRoute.snapshot.params.id)
      .expand('course,studyProgramSpecialty,program($expand=department,studyLevel)')
      .getItems()
    this.courseId = this._activatedRoute.snapshot.params.id;
  }

}
