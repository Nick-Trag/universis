import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassesHomeComponent } from './components/classes-home/classes-home.component';
import {ClassesTableComponent } from './components/classes-table/classes-table.component';
import { ClassesRootComponent } from './components/classes-root/classes-root.component';
import { ClassesPreviewComponent } from './components/classes-preview/classes-preview.component';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule} from '@ngx-translate/core';
import {ClassesRoutingModule} from './classes.routing';
import {ClasseSharedModule} from './classes.shared';
import { ClassesPreviewGeneralComponent } from './components/classes-preview-general/classes-preview-general.component';
import { ClassesPreviewStudentsComponent } from './components/classes-preview-students/classes-preview-students.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {StudentsSharedModule} from '../students/students.shared';
import {InstructorsSharedModule} from '../instructors/instructors.shared';
import { ClassesPreviewInstructorsComponent } from './components/classes-preview-instructors/classes-preview-instructors.component';
import {ElementsModule} from '../elements/elements.module';
import {ClassesAdvancedTableSearchComponent} from './components/classes-table/classes-advanced-table-search.component';
// tslint:disable-next-line: max-line-length
import {ClassesStudentAdvancedTableSearchComponent} from './components/classes-preview-students/classes-student-advanced-table-search.component';
import {MostModule} from '@themost/angular';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ClasseSharedModule,
    TablesModule,
    ClassesRoutingModule,
    SharedModule,
    FormsModule,
    StudentsSharedModule,
    InstructorsSharedModule,
    ElementsModule,
    MostModule
  ],
  declarations: [ClassesHomeComponent,
    ClassesPreviewComponent,
    ClassesRootComponent,
    ClassesTableComponent,
    ClassesPreviewGeneralComponent,
    ClassesPreviewStudentsComponent,
    ClassesPreviewInstructorsComponent,
    ClassesAdvancedTableSearchComponent,
    ClassesStudentAdvancedTableSearchComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClassesModule { }
