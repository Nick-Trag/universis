import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesPreviewInstructorsComponent } from './classes-preview-instructors.component';

describe('ClassesPreviewInstructorsComponent', () => {
  let component: ClassesPreviewInstructorsComponent;
  let fixture: ComponentFixture<ClassesPreviewInstructorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesPreviewInstructorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesPreviewInstructorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
