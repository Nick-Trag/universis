import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classes-root',
  template: '<router-outlet></router-outlet>',
})
export class ClassesRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
