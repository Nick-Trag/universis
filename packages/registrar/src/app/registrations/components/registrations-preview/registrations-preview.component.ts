import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-registrations-preview',
  templateUrl: './registrations-preview.component.html'
})
export class RegistrationsPreviewComponent implements OnInit {

  @Input() StudentPeriodRegistration: any;
  @Input() RegistrationTitle : String;

   constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private translate: TranslateService) {
  }

  async ngOnInit() {

    this.StudentPeriodRegistration = await this._context.model('StudentPeriodRegistrations')
    // .asQueryable()
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .expand('registrationPeriod,student($expand=person)')
    .getItem();

    this.RegistrationTitle = this.StudentPeriodRegistration.student.person.familyName + ' ' + this.StudentPeriodRegistration.student.person.givenName + ' - ' + this.translate.instant('Periods.' + this.StudentPeriodRegistration.registrationPeriod.alternateName) + ' ' + this.StudentPeriodRegistration.registrationYear.alternateName; 
  }

}
