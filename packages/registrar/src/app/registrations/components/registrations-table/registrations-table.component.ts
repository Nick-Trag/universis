import { Component, OnInit, ViewChild } from '@angular/core';
import * as REGISTRATIONS_LIST_CONFIG from './registrations-table.config.json';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registrations-table',
  templateUrl: './registrations-table.component.html',
  styleUrls: ['./registrations-table.component.scss']
})
export class RegistrationsTableComponent implements OnInit {

  
  public readonly config = REGISTRATIONS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
