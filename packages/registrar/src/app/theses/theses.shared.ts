import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {ThesesPreviewFormComponent} from './components/theses-preview-general/theses-preview-form.component';
import {SharedModule} from '@universis/common';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
     ThesesPreviewFormComponent
  ],
  exports: [
    ThesesPreviewFormComponent
  ]
})
export class ThesesSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading instructors shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/theses.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
