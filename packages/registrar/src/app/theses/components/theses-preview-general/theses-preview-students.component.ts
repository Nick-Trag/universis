import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-theses-preview-students',
  templateUrl: './theses-preview-students.component.html',
})
export class ThesesPreviewStudentsComponent implements OnInit {
  public model: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this.model = await this._context.model('Theses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('instructor,type,status,students($expand=student($expand=department,person,studentStatus))')
      .getItem();
  }

}

