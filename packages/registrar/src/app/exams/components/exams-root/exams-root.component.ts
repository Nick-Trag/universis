import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-exams-root',
  templateUrl: './exams-root.component.html'
})
export class ExamsRootComponent implements OnInit {
  public model: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext)  { }

  async ngOnInit() {
    this.model = await this._context.model('CourseExams')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .select('id', 'name')
      .getItem();
  }


}
