import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructors-dashboard-overview-supervised',
  templateUrl: './instructors-dashboard-overview-supervised.component.html',
  styleUrls: ['./instructors-dashboard-overview-supervised.component.scss']
})
export class InstructorsDashboardOverviewSupervisedComponent implements OnInit {
  @Input() id: any;
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('StudentTheses')
      .asQueryable()
      .expand('thesis($expand=instructor),student($expand=person)')
      .where('thesis/instructor/id').equal(this.id)
      .getItems();
  }

}
