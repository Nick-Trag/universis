import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from 'packages/registrar/src/app/tables/components/advanced-table/advanced-table-search-base';

@Component({
    selector: 'app-instructors-classes-advanced-table-search',
    templateUrl: './instructors-classes-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class InstructorsClassesAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
