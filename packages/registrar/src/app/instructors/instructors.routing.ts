import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstructorsHomeComponent} from './components/instructors-home/instructors-home.component';
import {InstructorsTableComponent} from './components/instructors-table/instructors-table.component';
import {InstructorsDashboardComponent} from './components/instructors-dashboard/instructors-dashboard.component';
import {InstructorsRootComponent} from './components/instructors-root/instructors-root.component';
import {InstructorsDashboardOverviewComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview.component';
import {InstructorsDashboardGeneralComponent} from './components/instructors-dashboard/instructors-dashboard-general/instructors-dashboard-general.component';
import {InstructorsDashboardClassesComponent} from './components/instructors-dashboard/instructors-dashboard-classes/instructors-dashboard-classes.component';
import {InstructorsDashboardExamsComponent} from './components/instructors-dashboard/instructors-dashboard-exams/instructors-dashboard-exams.component';
import {InstructorsDashboardThesesComponent} from './components/instructors-dashboard/instructors-dashboard-theses/instructors-dashboard-theses.component';


const routes: Routes = [
  {
    path: '',
    component: InstructorsHomeComponent,
    data: {
      title: 'Instructors'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: InstructorsTableComponent,
        data: {
          title: 'Instructors List'
        }
      }
    ]
  },
  {
    path: ':id',
    component: InstructorsRootComponent,
    data: {
      title: 'Instructor Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: InstructorsDashboardComponent,
        data: {
          title: 'Instructor Dashboard'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: InstructorsDashboardOverviewComponent,
            data: {
              title: 'Instructors.Overview'
            }
          },
          {
            path: 'general',
            component: InstructorsDashboardGeneralComponent,
            data: {
              title: 'Instructors.General'
            }
          },
          {
            path: 'classes',
            component: InstructorsDashboardClassesComponent,
            data: {
              title: 'Instructors.Classes'
            }
          },
          {
            path: 'exams',
            component: InstructorsDashboardExamsComponent,
            data: {
              title: 'Instructors.Exams'
            }
          },
          {
            path: 'theses',
            component: InstructorsDashboardThesesComponent,
            data: {
              title: 'Sidebar.Theses'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class InstructorsRoutingModule {
}
