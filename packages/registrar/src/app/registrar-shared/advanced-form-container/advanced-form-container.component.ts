import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-advanced-form-container',
  templateUrl: './advanced-form-container.component.html'
})
export class AdvancedFormContainerComponent implements OnInit {

  @Input() model: any;
  @Input() src: any;

  constructor() { }

  ngOnInit() {
    //
  }

}
