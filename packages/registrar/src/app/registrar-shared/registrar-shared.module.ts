import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppSidebarService} from './services/app-sidebar.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {ApplicationSettingsConfiguration, ErrorModule} from '@universis/common';
import {ActiveDepartmentService} from './services/activeDepartmentService.service';
import { AdvancedFormContainerComponent } from './advanced-form-container/advanced-form-container.component';
import {AdvancedFormsModule} from '@universis/forms';
import { AdvancedFormRouterComponent } from './advanced-form-router/advanced-form-router.component';
import {RouterModule} from '@angular/router';
import {MostModule} from '@themost/angular';

export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  useDigitalSignature: boolean;
}

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      AdvancedFormsModule,
      ErrorModule,
      MostModule
  ],
  declarations: [
    AdvancedFormContainerComponent,
    AdvancedFormRouterComponent
  ],
  providers: [
  ],
  exports: [
    AdvancedFormContainerComponent,
    AdvancedFormRouterComponent
  ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class RegistrarSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: RegistrarSharedModule,
              private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading registrar shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RegistrarSharedModule,
      providers: [
        AppSidebarService,
        ActiveDepartmentService
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

}
