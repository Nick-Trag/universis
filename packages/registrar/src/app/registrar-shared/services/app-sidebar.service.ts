import {ChangeDetectorRef, Injectable} from '@angular/core';
import {TranslateService, TranslatePipe} from '@ngx-translate/core';
import * as APP_SIDEBAR_ITEMS from './app-sidebar.service.json';
export declare interface AppSidebarNavigationItem {
  name: string;
  url: string;
  class: string;
  index?: number;
  icon?: string;
  children?: Array<AppSidebarNavigationItem>;
}

class TranslationChangeDetector extends ChangeDetectorRef {
  checkNoChanges(): void {
  }

  detach(): void {
  }

  detectChanges(): void {
  }

  markForCheck(): void {
  }

  reattach(): void {
  }

}

@Injectable({
  providedIn: 'root'
})
export class AppSidebarService {

  public navigationItems: Array<AppSidebarNavigationItem> = [];
  private readonly _changeDetector: TranslationChangeDetector;
  constructor(private _translateService: TranslateService) {
    this._changeDetector = new TranslationChangeDetector();
    const items: any = APP_SIDEBAR_ITEMS;
    this.addRange(items);
  }

  /**
   * Adds one or more navigation items to application sidebar
   * @param item
   */
  public add(...item: AppSidebarNavigationItem[]): void {
      // get translations keys
    const keys = item.map( x => {
      return x.name;
    });
    // initialize translate pipe with null change detector
    const pipe = new TranslatePipe(this._translateService, this._changeDetector);
    // add navigation items to array
    this.navigationItems.push.apply(this.navigationItems, item.map(navigationItem => {
      // translate name
      const x = Object.assign(navigationItem, {
        name: pipe.transform(navigationItem.name)
      });
      if (Array.isArray(x.children)) {
        // translate children
        x.children = x.children.map( child => {
          return Object.assign(child, {
            name: pipe.transform(child.name)
          });
        });
      }
      return x;
    }));
  }

  /**
   * Adds a collection of navigation items to application sidebar
   * @param items
   */
  public addRange(items: Array<AppSidebarNavigationItem>): void {
    return this.add.apply(this, items);
  }

}
