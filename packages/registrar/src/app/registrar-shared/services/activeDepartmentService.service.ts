import {UserStorageService} from '@universis/common/shared/services/user-storage';
import {EventEmitter, Injectable, Output} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ActiveDepartmentService {
  // Observable navItem source
  private _activeDepartmentSource = new BehaviorSubject<any>(0);
  // Observable navItem stream
  public departmentChange = this._activeDepartmentSource.asObservable();

  private activeDepartment: any;

  constructor( private _userStorage: UserStorageService) {
  }

  // get the active department
  async getActiveDepartment() {
    if (this.activeDepartment) {
      return this.activeDepartment.value;
    }
    const storageKey = 'registrar/departments/active';
    this.activeDepartment = await this._userStorage.getItem(storageKey);
    this._activeDepartmentSource.next(this.activeDepartment.value);
    return this.activeDepartment.value;
  }

  setActiveDepartment( activeDepartment: any ) {
    const storageKey = 'registrar/departments/active';
    this._userStorage.setItem(storageKey, JSON.parse(JSON.stringify(activeDepartment)));
    this.activeDepartment = { value: activeDepartment };
    this._activeDepartmentSource.next(activeDepartment);
  }
}
