import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Subscription} from 'rxjs';
import {AdvancedFormContainerComponent} from '../advanced-form-container/advanced-form-container.component';
import {AngularDataContext} from '@themost/angular';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-advanced-form-router',
  templateUrl: '../advanced-form-container/advanced-form-container.component.html',
  styles: []
})
export class AdvancedFormRouterComponent extends AdvancedFormContainerComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _errorService: ErrorService) {
    super();
  }

  ngOnInit() {
    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
    ).subscribe( (results: any) => {
      const params = Object.assign({}, results[0], results[1]);
      // if params has a model defined e.g. LocalDepartments
      // and an action e.g. edit
        if (params.model && params.action) {
          // set form src
          this.src = `${params.model}/${params.action}`;
          if (params.id) {
            // try to get model data
            this._context.model(params.model).asQueryable(params.serviceParams).prepare()
              .where('id').equal(params.id).getItem().then( item => {
              this.model = item;
            }).catch( err => {
              this._errorService.showError(err);
            });
          }
        }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
