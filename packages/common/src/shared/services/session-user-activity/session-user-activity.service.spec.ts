import { TestBed, inject } from '@angular/core/testing';

import { SessionUserActivityService } from './session-user-activity.service';
import { UserActivityService } from './../user-activity/user-activity.service';

describe('SessionUserActivityService', () => {
  beforeAll(() => {
    window.sessionStorage.clear();
  });

  let sessionUserActivityService;

  const mockEntry1 = {
  type: 'type1',
  url: 'url1',
  description: 'desc1',
  dateCreated: new Date()
  };

  const mockEntry2 = {
    type: 'type2',
    url: 'url2',
    description: 'desc2',
    dateCreated: new Date()
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SessionUserActivityService,
        {
          provide: UserActivityService,
          useClass: SessionUserActivityService
        }
      ]
    });
  });

  afterEach(() => {
    window.sessionStorage.clear();
  });

  it('should be created', inject([UserActivityService], async (userActivity: UserActivityService) => {
    expect(userActivity).toBeTruthy();
    expect(userActivity instanceof SessionUserActivityService).toBeTruthy();
  }));

  describe('given a clean sessionStorage', () => {

    beforeEach(() => {
      sessionUserActivityService = TestBed.get(SessionUserActivityService);
    });

    it('should write to sessionStorage', () => {
      const payload = mockEntry1;
      const newValue = [{ ...mockEntry1, dateCreated: mockEntry1.dateCreated.toISOString() }];

      sessionUserActivityService.setItem(payload);
      const rawList = sessionStorage.getItem('userActivity');
      const parsedList = JSON.parse(rawList);

      expect(parsedList).toEqual(newValue);
    });
  });
});
