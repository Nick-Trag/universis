import { TestBed } from '@angular/core/testing';
import { UserActivityService } from './user-activity.service';

describe('UserActivityService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [UserActivityService]
    })
  );

  it('should be created', () => {
    const service: UserActivityService = TestBed.get(UserActivityService);
    expect(service).toBeTruthy();
  });

  describe('by calling the setItem function,', () => {
    let service;

    const mockEntry1 = {
      category: 'category1',
      url: 'url1',
      description: 'desc1',
      dateCreated: new Date()
    };

    const mockEntry2 = {
      category: 'category2',
      url: 'url2',
      description: 'desc2',
      dateCreated: new Date()
    };

    const mockEntry3 = {
      category: 'category3',
      url: 'url3',
      description: 'desc3',
      dateCreated: new Date()
    };

    beforeEach(() => {
      service = TestBed.get(UserActivityService);
    });

    it('should populate the list of entries', async () => {
      const payload = mockEntry1;
      const newValue = [payload];

      service.setItem(payload);
      expect(await service.getItems()).toEqual(newValue);
    });

    it('should respect past values.', async() => {
      const pastValue = [mockEntry1, mockEntry2];
      const payload = mockEntry3;
      const newValue = [payload, ...pastValue];

      service.list = pastValue;
      service.setItem(payload);
      expect(await service.getItems()).toEqual(newValue);
    });

    it('should not add more than 10 items', () => {
      const pastValue = [
        { ...mockEntry1, url: 0 },
        { ...mockEntry1, url: 1 },
        { ...mockEntry1, url: 2 },
        { ...mockEntry1, url: 3 },
        { ...mockEntry1, url: 4 },
        { ...mockEntry1, url: 5 },
        { ...mockEntry1, url: 6 },
        { ...mockEntry1, url: 7 },
        { ...mockEntry1, url: 8 },
        { ...mockEntry1, url: 9 }
      ];

      const payload = {...mockEntry2};

      const newValue = [
        payload,
        { ...mockEntry1, url: 0 },
        { ...mockEntry1, url: 1 },
        { ...mockEntry1, url: 2 },
        { ...mockEntry1, url: 3 },
        { ...mockEntry1, url: 4 },
        { ...mockEntry1, url: 5 },
        { ...mockEntry1, url: 6 },
        { ...mockEntry1, url: 7 },
        { ...mockEntry1, url: 8 }
      ];

      service.list = pastValue;
      service.setItem(payload);
      expect(service.list).toEqual(newValue);
    });

    it('should remove entries with the same url', async() => {
      const pastValue = [mockEntry1, mockEntry2, mockEntry1];
      const payload = mockEntry3;
      const newValue = [payload, mockEntry1, mockEntry2];

      service.list = pastValue;
      service.setItem(payload);
      expect(await service.getItems()).toEqual(newValue);
    });

    it('should throw error when parameter is null', async() => {
      const pastValue = [mockEntry1, mockEntry2];
      const payload = null;

      service.list = pastValue;
      service.setItem(payload)
        .then(() => {
          fail('Entry may not be null');
        })
        .catch((err) => {
          expect(err).toEqual(new Error('Entry may not be null'));
        });

      expect(await service.getItems()).toEqual(pastValue);
    });

    it('should throw error when URL  of the parameter is null', async() => {
      const pastValue = [mockEntry1, mockEntry2];
      const payload = {...mockEntry3, url: undefined};

      service.list = pastValue;
      service.setItem(payload)
        .then(() => {
          fail('Should throw exception.');
        })
        .catch((err) => {
          expect(err).toEqual(new Error('Entry URL may not be empty'));
        });

      expect(await service.getItems()).toEqual(pastValue);
    });
  });
});
