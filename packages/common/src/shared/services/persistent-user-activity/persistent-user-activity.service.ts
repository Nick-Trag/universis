import { Injectable } from '@angular/core';
import { UserStorageService } from './../user-storage';
import { UserActivityService, UserActivityEntry } from './../user-activity/user-activity.service';

/**
 *
 * PersistentUserActivityService
 *
 * Handles and store the user activity at the userStorage
 *
 */
@Injectable()
export class PersistentUserActivityService extends UserActivityService {

  private initialized = false;

  constructor(private userStorage: UserStorageService) {
    super();
  }

  /**
   *
   * Fetches existing userActivity data from the userStorage
   *
   */
  private async initialize(): Promise<void> {
    const hasUserStorage = await this.userStorage.hasUserStorage();

    if (!hasUserStorage) {
      throw new Error('UserStorage is not found.');
    }

    const userActivity = await this.userStorage.getItem('registrar/userActivity');

    if (userActivity && userActivity.value) {
      this.list = userActivity.value;
      this.initialized = true;
    }
  }

  /**
   *
   * Adds a new user activity entry on userStorage
   *
   * @param {UserActivityEntry} entry The entry to be stored
   *
   */
  public async setItem(entry: UserActivityEntry): Promise<void>  {
    if (!this.isInitialized()) {
      await this.initialize();
    }

    await super.setItem(entry);
    await this.userStorage.setItem('registrar/userActivity', this.list);
  }

  /**
   *
   * @override
   * Get the list of user activity entries
   *
   * @returns {Array<UserActivityEntry>} The list of user activity entries
   */
  public async getItems(): Promise<Array<UserActivityEntry>> {
    if (!this.isInitialized()) {
      await this.initialize();
    }

    return this.list;
  }

  /**
   *
   * Get the initialization status of the service.
   * When the service is initialized, the userActivity is fetched and the
   * service is ready to use
   *
   * @returns {boolean} A flag to tell whether the service is ready to use
   *
   */
  public isInitialized(): boolean {
    return this.initialized;
  }
}
