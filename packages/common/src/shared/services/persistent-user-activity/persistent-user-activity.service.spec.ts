import { TestBed, inject } from '@angular/core/testing';
import { UserStorageService } from './../user-storage';
import { AngularDataContext, MostModule } from '@themost/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserActivityService, UserActivityEntry } from './../user-activity/user-activity.service';
import { ApiTestingController } from './../../../../testing/src/api-testing-controller.service';
import { ApiTestingModule } from '../../../../testing/src/api-testing.module';
import { TestRequest } from '@angular/common/http/testing';
import { PersistentUserActivityService } from './persistent-user-activity.service';

describe('PersistentUserActivityService', () => {

  const mockEntry1: UserActivityEntry = {
    category: 'type1',
    url: 'url1',
    description: 'desc1',
    dateCreated: new Date()
  };

  let mockApi: ApiTestingController;
  beforeEach(
    () => {
      TestBed.configureTestingModule({
        imports: [
          CommonModule,
          FormsModule,
          MostModule.forRoot({
            base: '/api/',
            options: {
              useMediaTypeExtensions: false
            }
          }),
          ApiTestingModule.forRoot()
        ],
        providers: [
          UserStorageService,
          PersistentUserActivityService,
            {
            provide: UserActivityService,
            useClass: PersistentUserActivityService
          }
        ]
      }).compileComponents();
      mockApi = TestBed.get(ApiTestingController);
    }
  );

  it('should be created', inject([AngularDataContext], (context: AngularDataContext) => {
    const service: PersistentUserActivityService = TestBed.get(PersistentUserActivityService);
    expect(service).toBeTruthy();
  }));

  it('should write at the userStorage', inject(
    [AngularDataContext],
    async (context: AngularDataContext) => {

    const expectedResultList = [mockEntry1];
    const service: PersistentUserActivityService = TestBed.get(PersistentUserActivityService);

    const userStorage = TestBed.get(UserStorageService);

    mockApi.match({
      url: '/api/diagnostics/services',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush(
        [
          {
            'serviceType': 'DiagnosticService'
          }, {
            'serviceType': 'UserStorageService',
            'strategyType': 'MongoUserStorageService'
         }
        ]
      );
    });


    mockApi.match({
      url: '/api/users/me/storage/set',
      method: 'POST'
    }).map((request: TestRequest) => {
      request.flush([]);
    });

    mockApi.match({
      url: '/api/users/me/storage/get',
      method: 'POST'
    }).map((request: TestRequest) => {
      request.flush(expectedResultList);
    });

    await service.setItem(mockEntry1);
    const savedRaw = await userStorage.getItem('userActivity');
    expect(savedRaw).not.toBeFalsy();
    expect(savedRaw).toEqual(expectedResultList);
  }));
});
