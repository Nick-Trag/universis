import { Injectable, Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';

export declare interface ShowErrorModalOptions {
  continueLink?: string;
  buttonText?: string;
  iconClass?: string;
}

@Component( {
  selector: 'universis-error-modal',
  template: `
    <div class="modal-header text-center">
      <h4 class="modal-title pull-left">{{ title }}</h4>
      <button type="button" class="close pull-right" aria-label="Close" (click)="hide()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body mt-0 mb-0 text-center">
      <i [ngClass]="iconClass" class="fa-6x text-secondary mb-2"></i>
      <div><span> {{ message }} </span></div>
    </div>
    <div class="modal-footer">
      <button type="button" (click)="hide()" class="btn btn-primary btn-ok" [translate]="buttonText"></button>

    </div>
  `})

  export class ErrorModalComponent implements OnInit {

    public code;
    @Input() continueLink = '/';
    @Input() message: string;
    @Input() title: string;
    @Input() iconClass = 'far fa-frown';
    @Input() buttonText = 'Error.Continue';
    @Input() error: any;

    constructor ( private _translateService: TranslateService,
                  public bsModalRef: BsModalRef,
                  private _router: Router ) {

    }

  hide() {
      this.bsModalRef.hide();
      if (this.continueLink == null) {
        return Promise.resolve();
      }
      if (this.continueLink === '.') {
        return Promise.resolve();
      }
      return this._router.navigate([this.continueLink]);
    }

    ngOnInit() {

      // get last error
      const error = this.error;
      // check error.code property
      if (error && typeof error.code === 'string') {
        this.code = error.code;
      } else if (error && typeof (error.status || error.statusCode) === 'number') {
        this.code = `E${error.status || error.statusCode}`;
      } else {
        this.code = 'E500';
      }
      if (error && typeof error.continue === 'string') {
        this.continueLink = error.continue;
      }

      this._translateService.get(this.code).subscribe((translation) => {
        if (translation) {
          this.title = translation.title;
          this.message = translation.message;
        } else {
          this._translateService.get('E500').subscribe((result) => {
            this.title = result.title;
            this.message = result.message;
          });
        }
      });
    }
  }


@Injectable()
export class ErrorService {

  private _lastError: any;

  constructor(private _router: Router, private _modalService: BsModalService) {
    //
  }

  /**
   * @param {*} error
   * @returns {Promise<boolean>}
   */
  navigateToError(error) {
    this.setLastError(error);
    // if error is an instance of HttpErrorResponse
    if (error instanceof HttpErrorResponse) {
      if (error && error.error && error.error.statusCode) {
        return this._router.navigate(['/error', error.error.statusCode]);
      }
      // navigate to specific error e.g. /error/401
      // this will allow application to override specific error pages and show custom errors
      return this._router.navigate(['/error', error.status]);
    }
    // otherwise show default error component
    return this._router.navigate(['/error']);
  }

  showError(error: any, options?: ShowErrorModalOptions) {
    const initialState = Object.assign({
      error: error
    }, options);
    this._modalService.show(ErrorModalComponent, {initialState});
  }

  /**
   * Sets last application error
   * @param {*=} err
   * @return ErrorService
   */
  setLastError(err?: any) {
    this._lastError = err;
    return this;
  }

  /**
   * Gets last application error
   * @return {any}
   */
  getLastError() {
    return this._lastError;
  }

}
