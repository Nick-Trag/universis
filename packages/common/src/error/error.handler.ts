import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class ErrorsHandler implements ErrorHandler {

  static handlerUnauthorizedError() {
    window.location.href = `#/error/401.1?continue=/auth/loginAs`;
  }
  static handlerConnectionError() {
    window.location.href = `#/error/408.1?continue=`;
  }
  static handlerConnectionTimeoutError() {
    window.location.href = `#/error/408?continue=`;
  }
  static handlerGatewayError() {
    window.location.href = `#/error/502?continue=`;
  }
  static handlerServiceError() {
    window.location.href = `#/error/503?continue=`;
  }

  handleError(error: any) {
      // write error to console (it may be omitted?)
      console.error('ERROR', error);
      // handle general connection timeout (where rejection.status is equal to 0)
      if (error.rejection && error.rejection.status === 0) {
          return ErrorsHandler.handlerConnectionError();
      }
      // handle http response errors
      if (error.rejection && error.rejection.status) {
          // handle 408 Connection Timeout
          if (error.rejection.status === 408) {
              return ErrorsHandler.handlerConnectionTimeoutError();
          }
          // handle 401 Unauthorized and 498 Invalid token
          if (error.rejection.status === 401 || error.rejection.status === 498) {
              return ErrorsHandler.handlerUnauthorizedError();
          }
          if (error.rejection.status === 502) {
              // handle 502 Bad Gateway
              return ErrorsHandler.handlerGatewayError();
          }
          if (error.rejection.status === 503) {
              // handle Service Unavailable
              return ErrorsHandler.handlerServiceError();
          }
      }
  }



}
