import {ApiTestingModule} from '../../../testing/src/api-testing.module';
import {ApiTestingController} from '../../../testing/src/api-testing-controller.service';
import {MostModule} from '@themost/angular';
import {CommonModule} from '@angular/common';
import {async, inject, TestBed} from '@angular/core/testing';
import {AuthenticationService} from './authentication.service';
import {ConfigurationService} from '../../shared/services/configuration.service';
import {TestingConfigurationService} from '../../../testing/src/testing-configuration.service';
import {TestRequest} from '@angular/common/http/testing';

describe('TestService', () => {

    let mockApi: ApiTestingController;
    beforeEach(async(() => {
        // noinspection JSIgnoredPromiseFromCall
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                MostModule.forRoot({
                    base: '/api/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                ApiTestingModule.forRoot()
            ],
            providers: [
                AuthenticationService,
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }
            ],
            declarations: [
            ],
        }).compileComponents();
        // get api test controller
        mockApi = TestBed.get(ApiTestingController);
    }));

    it('should use authentication callback', inject([AuthenticationService],
        async (authService: AuthenticationService) => {
        // add match url
        mockApi.match({
            url: '/api/users/me/',
            method: 'GET'
        }).map((request: TestRequest) => {
            // register request mapper and return data
            request.flush({
                id: 1,
                name: 'user@example.com',
                description: 'Test User',
                groups: [
                    {
                        name: 'Administrators'
                    }
                ]
            });
        });
        // call authentication service callback
        const result = await authService.callback('a-testing-token--');
        expect(result).toBeTruthy();
        expect(result.name).toBe('user@example.com');
    }));

    it('should throw token expired error', inject([AuthenticationService],
        async (authService: AuthenticationService) => {
            // add match url
            mockApi.match({
                url: '/api/users/me/',
                method: 'GET'
            }).map((request: TestRequest) => {
                // register request mapper and return data
                request.error(new ErrorEvent('Token Not Found'), {
                   status: 404,
                   statusText: 'Token Not Found'
                });
            });
            // call authentication service callback
            authService.callback('a-testing-token--').then( result => {
                expect(false).toBe(true, 'Expected an error to be thrown.');
            }).catch( err => {
                expect(err).toBeTruthy();
                expect(err.status).toBe(404);
            });
        }));

    it('should throw forbidden error', inject([AuthenticationService],
        async (authService: AuthenticationService) => {
            // add match url
            mockApi.match({
                url: '/api/users/me/',
                method: 'GET'
            }).map((request: TestRequest) => {
                // register request mapper and return data
                request.flush(null);
            });
            // call authentication service callback
            authService.callback('a-testing-token--').then( result => {
                expect(false).toBe(true, 'Expected an error to be thrown.');
            }).catch( err => {
               expect(err).toBeTruthy();
               expect(err.message).toBe('Unauthorized');
            });
        }));

});
