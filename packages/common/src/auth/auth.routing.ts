import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthCallbackComponent } from './auth-callback.component';
import { LogoutComponent } from './components/logout/logout.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: 'auth',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
      },
      {
        path: 'login', component: LoginComponent, data: {
          title: 'Login'
        }
      },
      {
        path: 'logout', component: LogoutComponent, data: {
          title: 'Logout'
        }
      },
      {
        path: 'loginAs', component: LogoutComponent, data: {
          title: 'LoginAsDifferentUser'
        }
      },
      {
        path: 'callback', data: {
          code: null
        }, component: AuthCallbackComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
