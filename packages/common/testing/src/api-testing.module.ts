import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ApiTestingController} from './api-testing-controller.service';
import {ApiTestingInterceptor} from './api-testing.interceptor';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ]
})
export class ApiTestingModule {
    constructor( @Optional() @SkipSelf() parentModule: ApiTestingModule) {
        //
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ApiTestingModule,
            providers: [
                ApiTestingController,
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiTestingInterceptor,
                    multi: true,
                    deps: [ApiTestingController]
                },
            ]
        };
    }
}

