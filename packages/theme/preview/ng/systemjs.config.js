// noinspection ThisExpressionReferencesGlobalObjectJS
(function (global) {
    System.config({
        transpiler: 'ts',
        typescriptOptions: {
            tsconfig: true
        },
        meta: {
            'typescript': {
                "exports": "ts"
            }
        },
        paths: {
            // paths serve as alias
            'npm:': 'https://unpkg.com/'
        },
        bundles: {
            "npm:rxjs-system-bundle@6.3.3/Rx.system.min.js": [
                "rxjs",
                "rxjs/*",
                "rxjs/operator/*",
                "rxjs/observable/*",
                "rxjs/scheduler/*",
                "rxjs/symbol/*",
                "rxjs/add/operator/*",
                "rxjs/add/observable/*",
                "rxjs/util/*"
            ]
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: 'app',
            'main': 'app/main',
            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/common/http':'npm:@angular/common/bundles/common-http.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
            // other libraries
            'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
            // typescript for compilation in the browser
            'ts':             'npm:plugin-typescript/lib/plugin.js',
            'typescript':     'npm:typescript/lib/typescript.js',
            '@coreui/angular': 'npm:@coreui/angular@2.4.4/bundles/coreui-angular.umd.js',
            'highlight.js': 'npm:highlight.js',
            'prettier': 'npm:prettier',
            'jquery': 'npm:jquery',
            'ngx-bootstrap/modal':'npm:ngx-bootstrap@4.2.0/modal/bundles/ngx-bootstrap-modal.umd.js',
            'ngx-bootstrap/utils':'npm:ngx-bootstrap@4.2.0/utils/bundles/ngx-bootstrap-utils.umd.js',
            'ngx-bootstrap/positioning':'npm:ngx-bootstrap@4.2.0/positioning/bundles/ngx-bootstrap-positioning.umd.js',
            'ngx-bootstrap/component-loader':'npm:ngx-bootstrap@4.2.0/component-loader/bundles/ngx-bootstrap-component-loader.umd.js',
            'ngx-bootstrap/datepicker': 'npm:ngx-bootstrap@4.2.0/datepicker/bundles/ngx-bootstrap-datepicker.umd.min.js',
            'ngx-bootstrap/chronos': 'npm:ngx-bootstrap@4.2.0/chronos/bundles/ngx-bootstrap-chronos.umd.min.js',
            'ngx-bootstrap/mini-ngrx': 'npm:ngx-bootstrap@4.2.0/mini-ngrx/bundles/ngx-bootstrap-mini-ngrx.umd.min.js',
            'ngx-highlightjs': 'npm:ngx-highlightjs',
            'smartwizard': 'npm:smartwizard'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            "app": {
                "main": "./app/main.ts",
                "defaultExtension": "ts",
                meta: {
                    "*.component.ts": {
                        loader: "system.component-loader.js"
                    }
                }
            },
            "rxjs": {
                defaultExtension: false
            }
        }
    });
})(this);
