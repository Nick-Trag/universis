import { Component, OnInit } from '@angular/core';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';
import {el} from './config/config';
@Component({
  selector: 'lib-datepicker',
  templateUrl: './datepicker.component.html',
  styles: []
})
export class DatepickerComponent implements OnInit {
  bsValue: Date = new Date();
  constructor(private _localeService: BsLocaleService) {
  }

  ngOnInit() {
    defineLocale('el', el);
    this._localeService.use('el');
  }

}
