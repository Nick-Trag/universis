import {LocaleData} from 'ngx-bootstrap/chronos';
export const el: LocaleData = {
  abbr: 'el',
  parentLocale: 'en',
  months: 'Ιανουάριος_Φεβρουάριος_Μάρτιος_Απρίλιος_Μάιος_Ιούνιος_Ιούλιος_Αύγουστος_Σεπτέμβριος_Οκτώβριος_Νοέμβριος_Δεκέμβριος'.split('_'),
  monthsShort:  'Ιαν_Φεβ_Μαρ_Απρ_Μαΐ_Ιουν_Ιουλ_Αυγ_Σεπ_Οκτ_Νοε_Δεκ'.split('_'),
  monthsParseExact: true,
  weekdays: 'Κυριακή_Δευτέρα_Τρίτη_Τετάρτη_Πέμπτη_Παρασκευή_Σάββατο'.split('_'),
  weekdaysShort: 'Κυ_Δε_Τρ_Τε_Πέ_Πα_Σά'.split('_'),
  weekdaysParseExact: true,
  longDateFormat: {
    LTS: 'h:mm:ss A',
    LT: 'h:mm A',
    L: 'DD/MM/YYYY',
    LL: 'D MMMM, YYYY',
    LLL: 'D MMMM, YYYY h:mm A',
    LLLL: 'dddd, D MMMM, YYYY h:mm A'
  }
};
