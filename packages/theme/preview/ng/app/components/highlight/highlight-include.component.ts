import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import hljs from 'highlight.js/lib/highlight';
import prettier from 'prettier/standalone';
import htmlParser from 'prettier/parser-html';
import typescriptParser from 'prettier/parser-typescript';
import xmlLanguage from 'highlight.js/lib/languages/xml';
import javascriptLanguage from 'highlight.js/lib/languages/javascript';
import typescriptLanguage from 'highlight.js/lib/languages/typescript';
import jQuery from 'jquery';
import {HttpClient, HttpResponse} from '@angular/common/http';
// register languages
hljs.registerLanguage('xml', xmlLanguage);
hljs.registerLanguage('javascript', javascriptLanguage);
hljs.registerLanguage('typescript', typescriptLanguage);


function escapeHTML(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;');
}

function unEscapeHTML(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe
    .replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&#039;/g, '\'');
}

function escapeTypescript(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe.replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;')
    .replace(/@/g , '&#064;')
    .replace(/\{/g, '&#123;')
    .replace(/\}/g, '&#125;')
    .replace(/\(/g, '&#40;')
    .replace(/\)/g, '&#41;')
    .replace(/\//g, '&#47;')
    .replace(/=/g, '&#61;')
    .replace(/_/g, '&#95;');
}

function unescapeTypescript(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe.replace(/&quot;/g, '"')
    .replace(/&#039;/g, '\'')
    .replace(/&#064;/g, '@')
    .replace(/&#123;/g, '{')
    .replace(/&#125;/g, '}')
    .replace(/&#40;/g, '(')
    .replace(/&#41;/g, ')')
    .replace(/&#47/g, '/')
    .replace(/&#61/g, '=')
    .replace(/&#95;/g, '_');
}

@Component({
  selector: 'app-highlight-include',
  template: `
      <div class="card">
          <div class="card-body">
              <button class="btn btn-copy btn-sm btn-secondary" (click)="copyCode()">Copy code</button>
              <pre><code #codeElement class="hljs"></code></pre>
          </div>
      </div>
  `,
  styles: [`
      .card-body {
          padding: 0;
      }

      pre {
          margin: 0;
      }

      code.hljs {
          padding: 1.5rem;
          font-size: 12px;
      }

      .btn-copy {
          position: absolute;
          top: 4px;
          right: 4px;
          opacity: 0.5;
      }
  `]
})
export class HighlightIncludeComponent implements OnInit {

  @Input() highlight: string;
  @ViewChild('codeElement') codeElement: ElementRef;

  public innerHTML: string;

  constructor(private element: ElementRef, private http: HttpClient) {
  }

  copyCode() {
    const selectBox = document.createElement('textarea');
    selectBox.style.position = 'fixed';
    selectBox.style.left = '0';
    selectBox.style.top = '0';
    selectBox.style.opacity = '0';
    selectBox.value = unEscapeHTML(this.codeElement.nativeElement.innerHTML);
    document.body.appendChild(selectBox);
    selectBox.focus();
    selectBox.select();
    document.execCommand('copy');
    document.body.removeChild(selectBox);
  }

  ngOnInit() {
    if (this.highlight) {
      const hashIndex = this.highlight.indexOf('#');
      let source = this.highlight;
      let sourceElement;
      if (hashIndex > 0) {
        source = this.highlight.substr(0, hashIndex);
        sourceElement = this.highlight.substr(hashIndex);
      }

      this.http.get(source, {responseType: 'text'}).toPromise().then((html: string) => {
        if (html) {
          const targetElement = sourceElement ? jQuery(html).find(sourceElement) : jQuery(html);
          if (targetElement) {
            // format html
            const innerHtml = escapeHTML(prettier.format(targetElement.html(), {
              parser: 'html',
              printWidth: 140,
              htmlWhitespaceSensitivity: 'ignore',
              useTabs: false,
              plugins: {parsers: htmlParser}
            }));
            if (this.codeElement) {
              this.codeElement.nativeElement.innerHTML = innerHtml;
              hljs.highlightBlock(this.codeElement.nativeElement);
            }
          }
        }
      }).catch(err => {
        console.log('ERROR', 'HIGHLIGHT', err);
        this.http.get(source, {responseType: 'text'}).subscribe((ts: string) => {
          this.codeElement.nativeElement.innerHTML = escapeTypescript(prettier.format(ts, {
            parser: 'typescript',
            printWidth: 140,
            useTabs: true,
            plugins: {parser: typescriptParser}
          }));
          hljs.highlightBlock(this.codeElement.nativeElement, typescriptLanguage);
        });
      });
    }
  }

}
