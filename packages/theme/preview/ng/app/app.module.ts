import {CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {AppSidebarModule} from '@coreui/angular';
import {FullLayoutComponent} from './layouts/full-layout.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app.routing';
import {CardsComponent} from './components/cards/cards.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppComponentsComponent} from './components/components.component';
import { ExpandableCardsComponent } from './components/expandable-cards/expandable-cards.component';
import { HighlightComponent } from './components/highlight/highlight.component';
import { ModalsComponent } from './components/modals/modals.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import {HighlightIncludeComponent} from './components/highlight/highlight-include.component';
import {HttpClientModule} from '@angular/common/http';
import {DatepickerComponent} from './components/datepicker/datepicker.component';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { WizardComponent } from './components/wizard/wizard.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule,
        AppRoutingModule,
        AppSidebarModule,
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        FormsModule
    ],
    declarations: [
        AppComponent,
        DatepickerComponent,
        FullLayoutComponent,
        AppComponentsComponent,
        CardsComponent,
        ExpandableCardsComponent,
        HighlightComponent,
        HighlightIncludeComponent,
        ModalsComponent,
        WizardComponent
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        }
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {

}
