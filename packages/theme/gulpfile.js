'use strict';

const gulp = require('gulp');
const del = require('del');
const path = require('path');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const uglifycss = require('gulp-uglifycss');
const sass = require('gulp-sass');
const connect = require('gulp-connect');
const packageImporter = require('node-sass-package-importer');
const postcss=require('gulp-postcss');
const postcssUrl = require("postcss-url");
sass.compiler = require('node-sass');

const plugins= [autoprefixer({
  browsers: [
    'last 2 versions',
    '> 5%'
  ]
}),
  postcssUrl ({url:'inline'})];
// start dev server
gulp.task('connect', function() {
    connect.server({
        root: './dist/preview',
        livereload: true,
        host: process.env.IP || 'localhost',
        port: process.env.PORT || 4000
    });
    return Promise.resolve();
});
// set live reload
gulp.task('html', function () {
    return gulp.src('./dist/preview/**/*')
        .pipe(connect.reload());
});

// build sass
gulp.task('sass', function () {
    return gulp.src('./scss/universis.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            importer: packageImporter()
        }).on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css/'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
});

// copy files before serve
gulp.task('serve:copy', function() {
    return gulp.src('./preview/**/*')
        .pipe(gulp.dest('./dist/preview'));
});

// build sass before serve
gulp.task('serve:sass', function () {
    return gulp.src('./scss/universis.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            importer: packageImporter(),
            errLogToConsole: true,
            debugInfo: true
        })).pipe(postcss(plugins))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/preview/assets/css'));
});

gulp.task('serve:clean', function() {
    return del('./dist/preview');
});

// watch files for live reload
gulp.task('watch', function() {
    gulp.watch('./preview/**/*', gulp.series('serve:copy', 'serve:sass','html'));
    gulp.watch('./scss/**/*.scss', gulp.series('serve:sass', 'html'));
    return Promise.resolve();
});
// serve dev server
// 1. copy ./preview/* to ./dist/preview/
// 2. start connect server
// 3. build sass to ./dist/preview/css/
// 4. watch file changes for live reload
gulp.task('serve', gulp.series('serve:clean', 'serve:copy', 'serve:sass', 'connect', 'watch'), function() {
    //
});

gulp.task('publish:copy', function() {
    return gulp.src('./preview/**/*')
        .pipe(gulp.dest('../../public/packages/theme'));
});

gulp.task('publish:clean', function() {
    return del('../../public/packages/theme/');
});

// copy assets
gulp.task('publish:css', function () {
    return gulp.src('./css/**/*')
        .pipe(gulp.dest('../../public/packages/theme/assets/css/'));
});

// publish to public folder
// 1. build sass to ./css/
// 2. copy ./preview/**/* to public folder
// 3. copy ./css/**/* to public folder
gulp.task('publish', gulp.series('sass', 'publish:copy', 'publish:css'));
